# Pamiętacie z matematyki twierdzenie Pitagorasa?
# Trójkąt pitagorejski, to trójkąt prostokątny, którego boki są wyrażone liczbami naturalnymi a, b, c związanymi warunkiem: a2+b2=c2.
#Uwzględnij, że kolejność danych nie musi mieć znaczenia! Tzn. długość C użytkownik może podać jako pierwszą wartość. Przypomnij sobie zadanie 3
# Przykładowe dane:
# nie-trójkąty: [6, 9, 20], [3, 6, 11], [31, 14, 17]
# trójkąty nie-pitagorejskie: [4, 4, 4], [31, 17, 16], [10, 5, 8]
# trójkąty pitagorejskie: [3, 4, 5], [6, 8, 10], [5, 12, 13], [9, 40, 41], [8, 15, 17]
# trójkąty egipskie: [9, 12, 15], [3, 4, 5], [15, 20, 25], [6, 8, 10]

# a) Poproś użytkownika o podanie długości boków A, B i C i sprawdź czy w ogóle możliwe jest utworzenie z nich trójkąta
a = float(input("Podaj długość boku 'a': "))
b = float(input("Podaj długość boku 'b': "))
c = float(input("Podaj długość boku 'c': "))

#sortujemy długości od namniejszej do największej
def check_triangle(a, b, c):
    if c < a:
        temp = c
        c = a
        a = temp
    if c < b:
        temp = c
        c = b
        b = temp
    if b < a:
        temp = a
        a = b
        b = temp

    print (a , b, c)
#Sprawdzamy, czy z tych długości można ułożyć trójkąt
    is_triangle = False

    if (a + b > c):
        print ("Długości a, b, c utworzą trójkąt")
        is_triangle = True
        check_pitagorejski(a, b, c)
    else:
        print("Nic z tego, to nie jest trójkąt")
        is_triangle = False


# c) Szczególnym przypadkiem jest trójkąt egipski o stosunkach długości 3:4:5. Sprawdź czy trójkąt pitagorejski jest trójkątem egipskim.
def check_egipski(a, b, c):
    if a % b %c == 3 % 4 % 5:
        print("To jest trójkąt egipski")
    else:
        print("To nie jest trójkąt egipski")



# b) Odpowiedz czy trójkąt jest trójkątem pitagorejskim.
def check_pitagorejski(a, b, c):
    if (a**2 + b**2 == c**2):
        print('Utworzyłeś trójkąt pitagorejski.')
        check_egipski(a, b, c)
    else:
        print('Z podanych długości boków nie mozna utworzyć trójkąta pitagorejskiego.')

check_triangle(a, b, c)
