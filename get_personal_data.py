# Zadanie 2
# Utwórz skrypt, który zapyta użytkownika o imię, nazwisko i numer telefonu, a następnie:
# Sprawdź czy imię i nazwisko składają się tylko z liter, a nr tel składa się wyłącznie z cyfr (wyświetl tę informację jako true/false)
# Użytkownicy bywają leniwi. Nie zawsze zapisują imię czy nazwisko z dużej litery – popraw ich
# Niektórzy podają numer telefonu z myślnikami lub z spacjami, usuń zbędne znaki z numeru
# Zakładając, że twoi użytkownicy noszą polskie imiona, sprawdź czy użytkownik jest kobietą
# Połącz dane w jeden ciąg personal za pomocą spacji
# Policz liczbę wszystkich znaków w napisie personal
# Podaj liczbę tylko liter w napisie personal
#  Podpowiedź – weź pod uwagę, że numery telefonów w Polsce są 9-cyfrowe

name = input("Podaj swoej imię: ")
surname = input("Podaj swoje nazwisko: ")
phone_number = input("Podaj swój numer telefonu: ")

print(name.isalpha(), surname.isalpha())
if name.isalpha():
    name = name.capitalize()

if surname.isalpha():
    surname = surname.capitalize()

phone_number = phone_number.replace("-", "")
phone_number = phone_number.replace(" ", "")
if phone_number.isdigit() and len(phone_number) == 9:
    print(True)
else:
    print(False)

personal = (name + " " + surname + " " + phone_number)
if name[-1] == 'A' or name[-1] == 'a':
    print("Pani " + personal)
else:
    print("Pan " + personal)

print('Ilość liter imienia i nazwiska: ' + str(len(name + surname)))