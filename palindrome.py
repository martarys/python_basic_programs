def is_palindrome(word):
    counter = 0
    word_len = len(word)

    for i in range(0, word_len //2):
        if word[i] != word[-i-1]:
            return False
    return True

def is_palindrome2(word):
    return word == word[::-1]

word = "kajak kajak"

print(is_palindrome(word))
