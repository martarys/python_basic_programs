# Zadanie 4
# Napisz skrypt obliczający wartość silnii. Rozwiąż zadanie za pomocą pętli for oraz pętli while.
# Wejście: „Podaj dowolną liczbę całkowitą do 15:” 4
# Wyjście: 4! = 24

a = int(input('Podaj liczbę, dla której chcesz obliczyc silnię \n'))
# i = 1
# wynik = 1
# while i <= a:
#     wynik = wynik * i
#     i += 1
# print(wynik)

silnia = 1
for i in range(1, a + 1):
    silnia = silnia * i
print(silnia)