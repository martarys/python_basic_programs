# Zadanie domowe
# Napisz program, który wystawi ocenę z testu.
# * `0 - 39` pkt - ocena niedostateczna
# * `40 - 54` pkt - ocena dopuszczająca
# * `55 - 69` pkt - ocena dostateczna
# * `70 - 84` pkt - ocena dobra
# * `85 - 98` pkt - ocena bardzo dobra
# * `99 - 100` pkt - ocena celująca
# 1. Stwórz zmienną `points`, do której będzie przypisany wynik z testu.
# 2. Na początku sprawdź czy ilość punktów jest większa bądź równa `0`, jeśli nie wypisz na stronie
# komunikat `Ilość punktów mniejsza niż 0.`
# 3. Na początku sprawdź czy ilość punktów jest mniejsza bądź równa `100`,
# nie wypisz na stronie komunikat `Ilość punktów większa niż 100.`
# 4. Następnie sprawdź jaka ocena odpowiada danej ilości punktów i wypisz ją na stronie wg. wzoru `Wynik: ocena dobra`
# 5. Rozwiąż to zadanie używając raz konstrukcji `if ... elif ... else`.

def wystawienie_ocen():

    points = float(input("Wpisz ilość zdobytych punktów: "))
    if points < 0 or points > 100:
        print("Błędny wynik")
        if points <= 0:
            print("Ilość punktów mniejsza niż 0.")
        else:
            print("Ilość punktów większa niż 100")
    else:
        if points >= 0 and points <= 39:
            print("Ocena niedostateczna")
        elif 40 <= points <= 54:
            print("Ocena dopuszcająca")
        elif 55 <= points <= 69:
            print("Ocena dostateczna")
        elif 70 <= points <= 84:
            print("Ocena dobra")
        elif 85 <= points <= 98:
            print("Ocena bardzo dobra")
        else:
            print("Ocena celująca")

    wystawienie_ocen()

wystawienie_ocen()