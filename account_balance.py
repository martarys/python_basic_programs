# Napisz skrypt, który, który obliczy stan konta za kilka lat. Program pyta użytkownika o:
# stan początkowy konta,
# stopę oprocentowania rocznego (zwróć uwagę, że odsetki podlegają miesięcznej kapitalizacji)
# liczbę lat na lokacie.
# Wynik wyświetl jako zdanie używając dowolnego sposobu formatowania tekstu. Wypisz np. takie zdanie:
# Twoje *stan_początkowy* zł przez *czas* lata na lokacie *oprocentowanie* % urośnie do *w

start = float(input("stan początkowy konta wynosi: "))
rate = float(input("Podaj stopę oprocentowania w procentach: ")) /100
n = int(input("Liczba lat: "))
end = start + (rate * start * n)

print("Twój stan konta początkowy {} po {} latach przy oprocentowaniu {} wyniesie {}".format(start, n, rate, end))
