# Zadanie 3 – sortowanie liczb
# Trzy dowolne liczby zapisz do trzech zmiennych.
# Znajdź największą liczbę.
# Wyświetl liczby od najmniejszej do największej.

def sort1(list):
    #zakładamy, że lista jest nieposortowana
    is_sorted = False

    while not is_sorted:
        i = 0
        is_sorted = True
        for element in lista:
            if (i < len(lista) - 1 and element > lista[i+1]):
                is_sorted = False
                temp = lista[i + 1]
                lista[i + 1] = lista[i]
                lista[i] = temp
            i += 1
    return lista

num1 = int(input("Podaj liczbę: "))
num2 = int(input("Podaj liczbę: "))
num3 = int(input("Podaj liczbę: "))

if (num1 > num2) and (num1 > num3):
    maximum = num1
elif (num2 > num1) and (num2 > num3):
    maximum = num2
else:
    maximum = num3
print("Największa liczba to: ", maximum)

lista = [num1, num2, num3]
print("Lista w kolejności od najmniejszej do największej: ", sort1(lista))
