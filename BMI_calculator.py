# Zadanie 2 – kalkulator BMI
# W poprzednich częściach pisaliśmy kalkulator BMI, teraz wzbogacimy go o interpretację wyniku.
# Program powinien sprawdzić czy BMI należy do jednego z 4 wyników: niedowaga/waga normalna/lekka
# nadwaga i nadwaga. Ponadto w przypadku nadwagi chcemy sprawdzić czy mamy doczynienia z otyłością.
# Poniżej tabela z klasyfikacją wyników: Niedowaga < 18,5, Waga normalna 18,5 – 24, Lekka nadwaga 24 – 26,5,
# Nadwaga > 26,5, Otyłość I stopnia 30 – 35, Otyłość II stopnia 30 – 40, Otyłość III stopnia > 40.


wzrost = float(input('Podaj swój wzrost w cm ')) / 100
waga = float(input("Podaj swoją masę "))
BMI = waga / (wzrost**2)

print("Twoje BMI wynosi: ", round(BMI, 4))
if (BMI < 18.5):
    print('Masz niedowagę')

elif (18.5 < BMI <= 24):
    print('Twoja waga jest prawidłowa')

elif (24 < BMI <= 26.5):
    print("Masz lekką nadwagę")

else:
    print('Twoja waga jest za wysoka')

    if (26.5 < BMI <= 30):
        print('Masz nadwagę')

    elif (30 < BMI <= 35):
        print('Masz otyłość I stopnia')

    elif (35 < BMI <= 40):
        print('Masz otyłość II stopnia')

    else:
        print('Masz otyłość III stopnia')
