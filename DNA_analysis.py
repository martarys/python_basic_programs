# Zadanie 3*
# Łatwiejsze niż się wydaje;
# Wyobraź sobie, że jesteś bioinformatykiem i otrzymujesz kod genetyczny do analizy.
# Kod DNA składa się z 4 zasad azotowych: adeniny(A), cytozyny(D), guaniny(G), tyminy(T).
# Idealny kod DNA wygląda następująco: TGCACGATCATGTCTACTATCCTCTCTATGGTGGGGTT.
# Zdarza się, jednak, że kod zawiera małe jak i duże litery. Kolejny problem to maszyny sekwencjonujące nie są wolne od błędów. W zależności
# od maszyny błędy sekwencjonowania mogą zostać zamienione na znak – czy literę N.
# W jaki sposób łatwo rozpoznasz, że otrzymany kod DNA zawiera błędy?
# Dotarł do ciebie następujący kod genetyczny:

# Skopiuj kod genetyczny do swojego skryptu i zapisz jako DNA = ACTG...
# Policz ile razy występuje w kodzie każda zasada azotowa – adenina, cytozyna, guanina, tymina.
# Na pewno zauważysz błędy sekwencjonowania – znaki, które nie są żadną z 4 zasad.
# W panice szukasz pliku z dokumentacją.
# Ufff… znalazł się!
# Co więcej, w dokumentacji pojawił się następujący zapis:
# gdy jakość sekwencji nie pozwala dokładnie odczytać rodzaju zasady azotowej wstawiany jest znak „-”
# Natomiast, gdy laser sczytujący ześlizgnie się, wstawiane są litery „N”, jednocześnie ostatnia wartość
# zasady jest ponownie odczytywana bez ubytku zasady w tym miejscu.
# Co za przydatna informacja!
# Policz wystąpienia sekwencji GAGA i zamień je na AGAG
# Znajdź miejsce (indeks) w łańcuchu, gdzie występuje 7 guanin z rzędu
# Znajdź miejsce (indeks) , gdzie od końca łańcucha występuje 6 cytozyn
# Policz ile razy w kodzie pojawiła się sekwencja CTGAAA
# W sekwencji CTGAAA czasem mutuje ostania litera A, wówczas jakość ostatniej litery może być wątplia. Ile sekwencji znajdziesz, jeśli weźmiesz
# pod uwagę wątpliwą, ostatnią adeninę?
# Oczyść DNA z wszystkich błędów. Na podstawie czystej nici utwórz odpowiadającą jej nić RNA (nić RNA w miejscu tyminy będzie mieć uracyl (U))

DNA = "ACTGTGCTGACTCCCGGTGCTGCCGCTGCCATAGCTAAAGCCCGGGTCCTGGTA \
GGCAGGCGGGAAGCAGGGTGGGGGTCCCGGGTACTGGTAGGGGTAGCCCTGACCC \
AGAGGCGGGGGGGCAGCCGGGTGGGGCAGCGGGGCCAGCGTGTCCTGAA-CGAAGT \
CCCACTGGAGCCACTGTTGAGGTTCAGGGTGGCGAGATCTGGCGGNNNAGGGTAGGTGA \
GGGCCGCGGAGGGGCCTCCGGCGTTCCCCTCCCCCCCGCCCTGAAACCCGAAGCCCCCA \
CTCACTGCTGCAGAGATCCCCTGAAAACGTAGTAGCACTGCTCgagacAGGTGATCTGTT  \
GACCTGAAACCGCAGGAAGCCGTGCTTCAGCAAGCTGCTGGCGTACTTCCGGGCCT---GCC \
GCTCCTTGAAGCCCTCCACGTGTGTGTACAGCCAGTCCACCACGTCCGCCCCTGGCCGGCACCA \
GCGGTCAGCCCGCAGCCTCGAGGCAAGCAGCCCTGCCNNTGGCACTATCCGC-CGCGGGGACGGCC \
ACTCACCGATGACGGCATNNGCGATGGTGATCTTGAGCCACATGCGGTCGCGGATCTCCAGTCCCG \
AG---GGCAGCTGCATGACCCGGACGACGGCGCTCATGTCACtcaccgtcagcggcgcctcttccagC  \
CAGCTCTGCAAAGCACAGACAGCCCCGCTTCGCCCCAGCATCTGAAAGCGGGGGACTCggcAcgCTG \
CACCCCCAGGGGAGCCTCTGGGCAGAGCCTGCGCCAGGGCGCAAGCTGGACGGTGCGTGACAGCAGGG \
CCCCGGCCCACTGCAGGATGCACCCCCGTGAGGCTGGGGCGTGAGCAGGGGGGTTGGACAtttAGTCTCC \
CACTTCTACAGACACTTTTCATCAGGATCCTAGGCACAAACTGGGCTGAAACCCCACCCTGCAGACCAGGAA \
GTAATGAGAACAGGGCAGGCCCCTTCCCCTCNNCGCATGCC-CACCCGAGAGCGCAGGCTGTTAGTCGTGTTAA \
TGGCAGGAAGCAGAATGGAGACCTGGCCCCTGCCTCTGAA-CCGTGGGTGCTCaactggctaGCCCTACGTACATC \
CCCTGTTCcggCCAACACACAGACATGAGCAGGATGGGCTGCACAAGGTGGGCACGGGTGCCTGTGCACACGTCTGTG \
CAGGGAGTTGGGGACAGGCAACACACACGTGTCACAGCCCCATGACGGggcaattgcGCCATGCTGGCTGAATGGCAGA \
GACGCCCCTCCAAGCCTCGGTTTCTGCTGGGGCCCTCAGGAGCTGCCACTTACGTGGAGCACCAGGCACGGAGCTGGTTAG \
TGAGGAGGAGCTGGTGCGCGTGACGGCGCTGGAGCAGGGACTCGTACCGTAGCGGGGCAGGGCNNNTGTCAGTGCCGCCGT \
GTGGtcagcggcgatCGGCG-GGTCGATGGGCCGCACCGGGTCAGCTGGGTGNAGACACGTGGCGATGACAGGCGGACAGATG \
GACAGGGTGGGAGGGCAGGGTGCAGGGCACAGAGGAGAGAGGCCTTCAGGCTAGGTAGGCGCCCCCTCCCCATCCCGccccGT \
GTGCCCCGAGGGCCACTCACCCCGTGGGACGGTGAAGTAGCTTCG-GGCGTTGGGTCCAGCACTTGGCCACAGTGAGGCTGNA \
AATGGCTGCAGGAACGGTGGTCCCCCCGCAAGGCCCCCATGGTCCCACCTCCCTGCCTGGCCCCTCCCGCTCCAGCGCCNCCAGCC"
DNA = DNA.upper()
DNA = DNA.replace(' ', '')
print(DNA, '\n')

A = DNA.count('A')
C = DNA.count('C')
T = DNA.count('T')
G = DNA.count('G')
print('adenina: {}, cytozyna: {}, tymina: {}, guanina: {}'.format(A, C, T, G) )

error1 = 'N'
error2 = '-'
print('Znaleziono: ' + str(DNA.count(error1)) + ' błędów')
print("Znaleziono {} błędów lasera oraz {} nieczytelnych zasad".format(DNA.count(error1), DNA.count(error2)))
print()

print('Sekwencja "CTGAAA" występuje w podanym ciągu ' + str(DNA.count('CTGAAA')))
print('Sekwencji "GAGA" występuje w podanym ciągu ' + str(DNA.count('GAGA')))

DNA = DNA.replace('GAGA', 'AGAG')
print(DNA.index('G'*7))
print(DNA.rindex('C'*6))

DNA = DNA.replace('N', '').replace('-', '')
print(DNA)

RNA = DNA.replace('T', 'U')
print(RNA)