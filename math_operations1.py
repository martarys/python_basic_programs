# Zadanie 4
# Napisz prosty program, który wykona się zadaną przez użytkownika liczbę razy.
# Z każdym uruchomieniem pętli wyświetli informacje:
# – czy liczba jest wielokrotnością 3
# – czy liczba jest wielkorotnością 4
# – wyświtli „hurra” jeżeli liczba dzieli się zarówno przez 3 jak i 4
# – wyświetli gwiazdkę, jeśli żaden z powyższych warunków nie jest spełniony

def pobieranie_liczby():
    i = int(input("Podaj liczbe całkowitą, naturalną: "))
    if i >= 0:
        print('Dziękujemy')
        num_check(i)
    else:
        print('to nie jest liczba całkowita, naturalna')
        pobieranie_liczby()


def num_check(i):
    if i% 3 == 0:
        print('Liczba jest wielokrotnościa liczby 3')
    if i% 4 == 0:
        print('Liczba jest wielokrotnościa liczby 4')
    if i% 3 == 0 and i% 4 == 0:
        print('Hurra')
    else:
        print('*')

pobieranie_liczby()