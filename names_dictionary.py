# Zadanie 4 – imiona
# Utwórz listę imion męskich / żeńskich.
# Użytkownik podaje imię.
# Jeśli imię istnieje na liście wyświetl czy jest to imię męskie czy żeńskie
#  Jeżeli nie dodaj imię do zbioru wraz z oznaczeniem płci.


dict_names = {
    'Marta':'żeńskie',
    'Alicja':'żeńskie',
    'Halina':'żeńskie',
    'Ewa':'żeńskie',
    'Marcin':'męskie',
    'Mikołaj':'męskie',
    'Henryk':'męskie'
}
user_name = input("Podaj swoje imię")

if user_name in dict_names.keys():
    print('Twoje imię jest', dict_names[user_name])
else:
    print('Nie znamy twojego imienia.')
    gender = input("To imię męskie czy żeńskie? wpisz m / z: ")
    if (gender == "m"):
        dict_names[user_name] = "męskie"

    else:

        dict_names[user_name] = "żeńskie"

    print(list(dict_names.keys()))