# Zadanie
# Utwórz spis oglądanych seriali.
# Każdy serial powinen mieć przypisaną ocenę w skali od 1-10.
# Zapytaj użytkownika jaki serial chce obejrzeć.
# W odpowiedzi podaj jego ocenę.
# Zapytaj użytkownika o dodanie kolejnego serialu i oceny.
# Dodaj serial do spisu.

moje_seriale = {"Stranger things": 4, "The Good place": 3, "Chicas de cable": 2, "Ania nie Anna": 1}
print(moje_seriale.keys())

pytanie = input("Jaki serial chcesz obejrzeć?")
print("Serial {} otrzymał ocenę {}".format(pytanie, moje_seriale[pytanie]))


new = input('Jaki serial dodać do bazy? ')
rating = input('Jaką ocenę otrzymał ' + new + '? ')
moje_seriale[new] = float(rating)

print('******************************************')
print(list(moje_seriale.keys()))
print(moje_seriale)